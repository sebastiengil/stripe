<?php //var_dump($_SESSION['cart']); ?>
<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">Article</th>
      <th scope="col">Echelle</th>
      <th scope="col">Description</th>
      <th scope="col">Prix unitaire</th>
      <th scope="col">Ajouter</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($data as $row){ ?>
    <tr>
        <td><?php echo($row->productName) ?></td>
        <td><?php echo($row->productScale) ?></td>
        <td><?php echo($row->productDescription) ?></td>
        <td><?php echo($row->buyPrice) ?>€</td>
        <td class="row">
            <?php
                $value = isset($_SESSION['cart'][$row->productCode]) ? $_SESSION['cart'][$row->productCode] : "0";
                echo form_open('addCart','id="form'.$row->productCode.'"');
                $input = array(
                    'type'  => 'number',
                    'name'  => $row->productCode,
                    'value' => $value,
                    'min'   => '0'
                );
                echo form_input($input).
                     form_close();
            ?>
            <button id="<?php echo($row->productCode) ?>" type="button" class="btn btn-light" data-toggle="popover" data-content="Ajouté au panier">Ajouter !</button>
        </td>
    </tr>
    <?php } ?>
  </tbody>
</table>