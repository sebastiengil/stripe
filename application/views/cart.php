<?php var_dump($products); ?>

<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">Article</th>
      <th scope="col">Prix unitaire</th>
      <th scope="col">Supprimer</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($data as $row){ ?>
    <tr>
        <td><?php echo($row->productName) ?></td>
        <td><?php echo($row->buyPrice) ?>€</td>
        <td><?php echo($qte[$row->productCode]) ?></td>
            <button id="<?php echo($row->productCode) ?>" type="button" class="btn btn-light" data-toggle="popover" data-content="Ajouté au panier">Supprimer !</button>
        </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
