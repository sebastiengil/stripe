<?php ?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="boutik">Home</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <?php if(isset($_SESSION["firstName"])){ ?>
        <li class="nav-item active">
          <a class="nav-link" href="cart">Panier <span class="sr-only">(current)</span>
            <span class="badge badge-warning" id="countForTheCart">
              <?php
                if(isset($_SESSION['cart'])){
                  echo(count($_SESSION['cart']));
                } else {
                  echo("0");
                }
              ?>
            </span>
          </a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="logout">Je me casse<span class="sr-only">(current)</span></a>
        </li>
      <?php } ?>
    </ul>
  </div>
</nav>

<section class="container">
    
