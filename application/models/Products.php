<?php

class Products extends CI_Model
{
    public function list(){
        $this->load->database();
        $query = 'SELECT * FROM products';
        $query = $this->db->query($query);
        return $query->result();
    }

    public function getById($id){
        $this->load->database();
        $query = 'SELECT productCode, productName, quantityInStock, buyPrice FROM products WHERE productCode="' .$id. "'";
        $query = $this->db->query($query);
        return $query->row();
    }
} 