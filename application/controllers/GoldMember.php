<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class GoldMember extends MY_Controller{
    
    public function signIn(){
        if(!$this->securitytkn->acceptConnexion()){
            $this->load->helper('form');
            $this->load->library('form_validation');
            $this->load->model('Employees');

            if($this->form_validation->run('login') == false){
                
                if($data = $this->input->post()){
                //var_dump((new Employees)->login($data));
                    $data = (new Employees)->login($data);
                    if($data->qte == 1){
                        $this->securitytkn->generateToken($data);
                        //var_dump($_COOKIE);
                        $_SESSION["firstName"] = $data->firstName;
                        redirect('boutik','GET');
                }
                    
                }

                $this->form_validation->set_error_delimiters('<p class="error">','</p>');
                $this->load->view('Template/Header');
                $this->load->view('SignForm');
                $this->load->view('Template/Footer');
                //var_dump($this->input->post());
                
            } else {

            }
        } else {
            redirect('/home','GET');
        }
    }

    public function signInUp(){
        if($this->securitytkn->acceptConnexion()){
            $this->load->view('Template/Header');
            $this->load->view('home');
            $this->load->view('Template/Footer');
        } else {
            redirect('/','GET');
        }
    }

    public function logOut(){
        $this->load->helper('form');
        $this->securitytkn->deactivate();
        session_destroy();
        redirect('/','GET');
    }



}