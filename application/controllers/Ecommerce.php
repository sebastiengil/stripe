<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ecommerce extends MY_Controller{
    
    public function products()
    {
        $this->load->database();
        $this->load->helper('url');
         $this->load->library('grocery_CRUD');

        $crud = new grocery_CRUD();
        $crud->set_table('products');
        $crud->columns('productName','productScale','productDescription','buyPrice');
        $crud->set_theme('datatables');
        $crud->unset_edit();
        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_print();
        $crud->unset_export();
        $crud->unset_read();
        $crud->add_action('Ajouter au panier', '', 'addCart','ui-icon-plus');

        
        //var_dump($crud);
        
        
        $output = $crud->render();
        //var_dump($crud);

        $this->_example_output($output);    
    }
 
    function _example_output($output = null)
    {
        $this->load->view('Template/Header');
        $this->load->view('products.php',$output);
        $this->load->view('Template/Footer');   
    }

    public function boutik(){
        if(!$this->securitytkn->acceptConnexion()){
            redirect('/','GET');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Products');

        $data = (new Products)->list();
        //var_dump($data);

        $data = array("data" => $data);
        //var_dump($data);

        $this->load->view('Template/Header');
        $this->load->view('boutik',$data);
        $this->load->view('Template/Footer');   

    }

    public function addCart(){
      
        if(!isset($_SESSION['cart'])){
            $_SESSION['cart'] = [];
        }
        foreach($this->input->post() as $post =>$postValue){
            $_SESSION['cart'][$post] = $postValue;
        }
        foreach($_SESSION['cart'] as $key =>$value){
            if($value == "0"){
                unset($_SESSION['cart'][$key]);
            }
        }
    }
    
    public function cart(){
        //echo(count($_SESSION['cart']));
        $tabCart = [];
        $qte = [];
        $sum = 0;
        $this->load->models('Products');
        $product = new Products;
        foreach($_SESSION['cart'] as $key =>$value){
            $res = $product->getById($key);
            $sum += ($res->buyPrice)*$value;
            $tabCart[$key] = $res;
            $qte[$key] = $value;
        }
        $tabCart = array(
            'products' => $tabCart,
            'qte' => $qte,
            'sum' => $sum
        );


        $this->load->view('Template/Header');
        $this->load->view('cart',$tabCart);
        $this->load->view('Template/Footer'); 
    }

}